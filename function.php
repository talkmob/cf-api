<?php 
include 'config.php';

function add_domain($domain,$email,$key) {
	$headers = array('X-Auth-Email: '.$email, 'X-Auth-Key: '.$key, 'Content-Type: application/json');
	$data = array('name' => $domain, 'jump_start' => false );

	//Add Domain

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, "https://api.cloudflare.com/client/v4/zones");
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

	$domain = json_decode(curl_exec($curl));
	$zone_id = $domain->result->id;
	curl_close($curl);
	return $domain;
}

function add_dns($domain,$type,$name,$value) {
	global $cf;
	return $cf->rec_new($domain, $type, $name, $value);
}

function redirect($source,$target,$email,$key) {
	$headers = array('X-Auth-Email: '.$email, 'X-Auth-Key: '.$key, 'Content-Type: application/json');
	// $data = array('name' => $domain, 'jump_start' => false );

	$data = array(
				'target' => array(
					'target' => 'url',
					'constraint' => array(
						'operator' => 'matches',
						'value' => '*'.$target.'/*'
					)
				),
				'actions' => array(
					'id' => 'always_online',
					'value' => 'on'
				)
			);

	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, 'https://api.cloudflare.com/client/v4/zones/'.md5($source).'/pagerules');
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));

	$redirect = json_decode(curl_exec($curl));
	curl_close($curl);
	return $redirect;
}
?>